defmodule StringTest do
  use ExUnit.Case, async: true

  import Config.String, only: [parse: 1]

  describe "Config.String.parse/1" do
    test "parses string as is" do
      assert parse("test me") == {:ok, "test me"}
    end

    test "parses integers" do
      assert parse(0)  == {:ok, "0"}
      assert parse(-1) == {:ok, "-1"}
      assert parse(10) == {:ok, "10"}
    end

    test "parses floats" do
      assert parse(0.5)  == {:ok, "0.5"}
    end

    test "parses booleans" do
      assert parse(true)   == {:ok, "true"}
      assert parse(false)  == {:ok, "false"}
    end

    test "parses atoms" do
      assert parse(:test_value) == {:ok, "test_value"}
    end
  end
end
