defmodule BooleanTest do
  use ExUnit.Case, async: true

  import Config.Boolean, only: [parse: 1]

  describe "Config.Boolean.parse/1" do
    test "parses booleans as is" do
      assert parse(true)  == {:ok, true}
      assert parse(false) == {:ok, false}
    end

    test "parses string representation" do
      assert parse("false") == {:ok, false}
      assert parse("fAlSe") == {:ok, false}
      assert parse("no")    == {:ok, false}
      assert parse("NO")    == {:ok, false}
      assert parse("0")     == {:ok, false}
      assert parse("true")  == {:ok, true}
      assert parse("yes")   == {:ok, true}
      assert parse("1")     == {:ok, true}
    end

    test "parses integers" do
      assert parse(0)  == {:ok, false}
      assert parse(1)  == {:ok, true}
      assert parse(-1) == {:ok, true}
      assert parse(10) == {:ok, true}
    end

    test "returns error for non-convertable values" do
      assert parse(1.2)          == :error
      assert parse({:test, :me}) == :error
      assert parse(test: :me)    == :error
      assert parse(%{test: :me}) == :error
    end
  end
end
