defmodule IntegerTest do
  use ExUnit.Case, async: true

  import Config.Integer, only: [parse: 1]

  describe "Config.Integer.parse/1" do
    test "parses integers as is" do
      assert parse(10) == {:ok, 10}
    end

    test "parses string representation" do
      assert parse("10") == {:ok, 10}
    end

    test "returns error on non-digit string" do
      assert parse("test") == :error
    end

    test "parses booleans" do
      assert parse(true)  == {:ok, 1}
      assert parse(false) == {:ok, 0}
    end

    test "parses floats by rounding it" do
      assert parse(1.2) == {:ok, 1}
      assert parse(2.5) == {:ok, 3}
    end

    test "returns error for non-convertable values" do
      assert parse({:test, :me}) == :error
      assert parse(test: :me)    == :error
      assert parse(%{test: :me}) == :error
    end
  end
end
