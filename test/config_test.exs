defmodule ConfigTest do
  use ExUnit.Case, async: true

  alias Config.Configuration
  alias Config.Category
  alias Config.Option

  import Config, only: [search_for: 1]

  defmodule A do
    use Config
    Config.config :app
  end

  defmodule B do
    use Config
    Config.config :app do
      option str_opt, string, default: "str_value"
      option int_opt, int, default: 100
    end
  end

  defmodule C do
    use Config
    Config.config :app do
      category root do
        option root_str_opt, str, [ROOT_STR, root, default: "root_str_value"]
        category a do
          option a_int_opt, int, [A_INT, i, default: 100]
        end
      end
    end
  end

  describe "Config.config/1" do
    test "forms __config__ function" do
      assert %{app: _} = A.__config__
      assert %Configuration{name: :app} = A.__config__(:app)
    end

    test "adds options" do
      opts = B.__config__(:app).children
      assert Enum.any?(opts, search_for(%Option{name: :str_opt, type: Config.String}))
      assert Enum.any?(opts, search_for(%Option{name: :int_opt, type: Config.Integer}))
    end

    test "adds categories" do
      root = C.__config__(:app).children |> List.first
      assert %Category{name: :root} = root
    end
  end
end
