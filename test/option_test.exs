defmodule ConfigOption do
  use ExUnit.Case, async: true

  alias Config.Option
  alias Config.CompileError

  describe "Option.compile/1" do
    test "it compiles name" do
      assert Option.compile(:test, :str, []).name == :test
      assert Option.compile(quote(do: test), :str, []).name == :test
    end

    test "it compiles type" do
      assert Option.compile(:test, :string, []).type == Config.String
      assert Option.compile(:test, quote(do: string), []).type == Config.String
      assert Option.compile(:test, :str, []).type == Config.String
      assert Option.compile(:test, quote(do: str), []).type == Config.String
      assert Option.compile(:test, :integer, []).type == Config.Integer
      assert Option.compile(:test, quote(do: integer), []).type == Config.Integer
      assert Option.compile(:test, :int, []).type == Config.Integer
      assert Option.compile(:test, quote(do: int), []).type == Config.Integer
      assert Option.compile(:test, :boolean, []).type == Config.Boolean
      assert Option.compile(:test, quote(do: boolean), []).type == Config.Boolean
      assert Option.compile(:test, :bool, []).type == Config.Boolean
      assert Option.compile(:test, quote(do: bool), []).type == Config.Boolean
    end

    test "it accepts custom types" do
      defmodule A, do: (def parse(_), do: :error)
      assert Option.compile(:test, quote(do: A), []).type == A
    end

    test "it avoids custom types without parse method" do
      defmodule B, do: ()
      assert_raise(CompileError, fn ->
        Option.compile(:test, quote(do: B), [])
      end)
    end

    test "it compiles command line switches" do
      assert Option.compile(:test, :str, ["sw"]).switches == ["--sw"]
      assert Option.compile(:test, :str, ["--sw"]).switches == ["--sw"]
      assert Option.compile(:test, :str, ["n"]).switches == ["-n"]
      assert Option.compile(:test, :str, ["n", "n"]).switches == ["-n"]
      assert Option.compile(:test, :str, ["-n"]).switches == ["-n"]
      assert Option.compile(:test, :str, ["n sw"]).switches == ["-n", "--sw"]
      assert Option.compile(:test, :str, ["n", "sw"]).switches == ["-n", "--sw"]
      assert Option.compile(:test, :str, switch: 0).switches == ["-0"]
      assert Option.compile(:test, :str, switch: true).switches == ["--test"]
      assert Option.compile(:test, :str, switch: false).switches == []
      assert Option.compile(:test, :str, switch: "n sw").switches == ["-n", "--sw"]
      assert Option.compile(:test, :str, [:switch]).switches == ["--test"]
      assert Option.compile(:test, :str, quote(do: [switch])).switches == ["--test"]
      assert Option.compile(:test, :str, quote(do: [n, sw])).switches == ["-n", "--sw"]
    end

    test "it avoids wrong switch option" do
      assert_raise(CompileError, fn ->
        Option.compile(:test, :str, switch: 0.5)
      end)
    end

    test "it compiles environment variables" do
      assert Option.compile(:test, :str, ["SOME_VAR"]).env == ["SOME_VAR"]
      assert Option.compile(:test, :str, quote(do: [SOME_VAR])).env == ["SOME_VAR"]
      assert Option.compile(:test, :str, env: "VAR AND OTHER").env == ["VAR", "AND", "OTHER"]
    end

    test "it avoids wrong env option" do
      assert_raise(CompileError, fn ->
        Option.compile(:test, :str, env: 0)
      end)
    end
  end
end
