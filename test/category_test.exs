defmodule CategoryTest do
  use ExUnit.Case, async: true

  alias Config.Category
  alias Config.Option
  alias Config.CompileError

  describe "Category.compile/1" do
    test "it compiles name" do
      assert Category.compile(:test, []).name == :test
      assert Category.compile(quote(do: test), []).name == :test
    end

    test "it compiles type" do
      assert Category.compile(:test, type: :map).type == :map
    end

    test "it rejects invalid types" do
      assert_raise(CompileError, fn ->
        Category.compile(:test, type: :undefined)
      end)
    end

    test "it collects child options" do
      category = Category.compile(:test, do: quote(do: option opt))
      assert %Option{name: :opt, type: Config.String} in category.children
    end

    test "it collects nested categories" do
      ast = quote do
        category a do
          category b do
            option opt
          end
        end
      end
      root = Category.compile(:root, do: ast)
      a = root.children |> List.first
      b = a.children    |> List.first
      assert %Category{name: :a} = a
      assert %Category{name: :b} = b
      assert %Option{name: :opt, type: Config.String} in b.children
    end
  end
end
