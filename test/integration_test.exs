defmodule IntegrationTest do
  use ExUnit.Case, async: false

  defmodule App do
    use Config
    Config.config :app do
      add_path Path.join([File.cwd!, "test", "support"])
      add_file "#{var!(name)}-config.#{var!(ext)}"
      category root do
        option root_str_opt, str, [ROOT_STR, root, default: "root_str_value"]
        category a do
          option a_int_opt, int, [A_INT, i, default: 100]
        end
      end
    end
  end

  setup do
    # cleanup app configuration
    Application.get_all_env(:app)
    |> Keyword.keys
    |> Enum.each(&(Application.put_env(:app, &1, nil)))
    # save env variables
    env  = System.get_env
    # save command line
    argv = System.argv
    on_exit fn ->
      # restore command line
      System.argv(argv)
      # unset dummy env variables
      (Map.keys(System.get_env) -- Map.keys(env)) |> Enum.each(&:os.unsetenv/1)
    end
  end

  describe "Config.load/1" do
    test "loads default values" do
      Config.load(App, :app)
      config = Application.get_env(:app, :root)
      assert Keyword.keyword?(config)
    end

    test "loads values from command line options" do
      System.argv(["--root", "argv_str", "-i", "1000"])
      Config.load(App, :app)
      config = Application.get_env(:app, :root)
      assert config[:a][:a_int_opt] == 1000
      assert config[:root_str_opt] == "argv_str"
    end

    test "loads values from env" do
      System.put_env("ROOT_STR", "env_str")
      System.put_env("A_INT", "10000")
      Config.load(App, :app)
      config = Application.get_env(:app, :root)
      assert config[:a][:a_int_opt] == 10000
      assert config[:root_str_opt] == "env_str"
    end

    test "loads values from yaml" do
      Config.load(App, :app)
      config = Application.get_env(:app, :root)
      assert config[:a][:a_int_opt] == 200
      assert config[:root_str_opt] == "yaml_root_str"
    end
  end
end
