defmodule Config.ConfigFile do
  @moduledoc false

  alias Config.Container
  alias Config.Option

  use Config.Loader

  def load({env, errors}, %{name: name, children: _} = config) do
    case Map.get(config, :paths) do
      nil ->
        {env, errors}
      paths ->
        folders = paths
                  |> Enum.uniq
                  |> Enum.reject(&(&1 == nil))
        env = config
              |> files(folders, available_formats)
              |> Enum.reduce(Container.create_value(config),
                             &(load_from(&1, config, &2)))
              |> Keyword.get(name, [])
              |> Container.merge_into(env, config)
        {env, errors}
    end
  end

  defp available_formats do
    with true <- Code.ensure_loaded?(:yamerl),
         :ok  <- Application.ensure_started(:yamerl) do
      %{yml: ~w(yml yaml)}
    else
      _ -> %{}
    end
  end

  defp files(%{name: name, files: files}, folders, formats) do
    for folder             <- folders,
        file               <- files,
        {format, ext_list} <- formats,
        ext                <- ext_list,
        {file, _} = Code.eval_quoted(file, [name: name, ext: ext]),
        file      = Path.join(folder, file),
        File.regular?(file) do
      {format, file}
    end
  end

  defp load_from({:yml, file}, config, out) do
    case :yamerl_constr.file(file) do
      [[el | _] = list] when is_list(el) ->
        Enum.reduce(list |> deserialize, out, &(load_yaml(&1, config, &2)))
      [[el | _] = kw] when is_tuple(el) ->
        load_yaml(kw |> deserialize, config, out)
      _ ->
        out
    end
  end

  defp deserialize(erl_value) when is_list(erl_value) do
    Enum.map(erl_value, &deserialize/1)
  end
  defp deserialize({k, v}) when is_list(k) do
    k = :erlang.list_to_binary(k)
    case v do
      [el | _] = v when is_integer(el) -> {k, :erlang.list_to_binary(v)}
      v -> {k, v |> deserialize}
    end
  end
  defp deserialize(x), do: x

  defp load_yaml(values, %{name: name, type: type, children: children}, out) do
    key = name |> to_string
    case values |> Enum.find_value(fn {^key, v} -> v; _ -> nil end) do
      nil   -> out
      value ->
        value = Enum.reduce(children,
                            Container.create_value(type),
                            &(load_yaml(value, &1, &2)))
        put_in(out, [name], value)
    end
  end
  defp load_yaml(values, %Option{name: name, type: type}, out) do
    key = name |> to_string
    case values |> Enum.find_value(fn {^key, v} -> v; _ -> nil end) do
      nil   -> out
      value ->
        case type.parse(value) do
          {:ok, value} -> put_in(out, [name], value)
          _            -> out
        end
    end
  end
end
