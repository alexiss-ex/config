defmodule Config.ParseError do
  @dialyzer {:nowarn_function, message: 1}
  defexception [:message]
end

defmodule Config.CompileError do
  @dialyzer {:nowarn_function, message: 1}
  defexception [:message]
end
