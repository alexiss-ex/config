defmodule Config.Env do
  @moduledoc false

  alias Config.Container
  alias Config.Option
  alias Config.ParseError

  use Config.Loader

  def load({env, errors}, %{children: children} = config) do
    env = children
          |> Enum.reduce(Container.create_value(config), &load_from/2)
          |> Container.merge_into(env, config)
    {env, errors}
  end

  defp load_from(%{name: name, type: type, children: children}, out) do
    value = children |> Enum.reduce(Container.create_value(type), &load_from/2)
    put_in(out, [name], value)
  end
  defp load_from(%Option{name: name, type: type, env: vars}, out) do
    Enum.reduce(vars, out, fn var, out ->
      with value when not is_nil(value) <- System.get_env(var) do
        case type.parse(value) do
          {:ok, value}  -> put_in(out, [name], value)
          :ignore       -> out
          :error        -> raise(ParseError)
          {:error, msg} -> raise(ParseError, msg)
        end
      else
        _ -> out
      end
    end)
  end
end
