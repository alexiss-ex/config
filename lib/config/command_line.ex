defmodule Config.CommandLine do
  @moduledoc false

  alias Config.Container
  alias Config.Option

  use Config.Loader

  def load({env, errors}, %{children: children} = config) do
    opts = [aliases: [], strict: [], refs: %{}]
    {opts, _} = children |> Enum.reduce({opts, []}, &collect_opts/2)
    {refs, opts} = opts |> Keyword.pop(:refs)
    env = System.argv
          |> Enum.join(" ")
          |> OptionParser.split
          |> OptionParser.parse(opts)
          |> put(env, config, refs)
    {env, errors}
  end

  defp put({out, _, _}, env, config, refs) do
    out = Enum.reduce(out, Container.create_value(config), fn {k, v}, out ->
      with path when is_list(path) <- Map.get(refs, k),
           %Option{type: type}     <- Container.get(config, path),
           {:ok, value}            <- type.parse(v) do
        Container.put(config, path, value, out)
      else
        _ -> out
      end
    end)
    out |> Container.merge_into(env, config)
  end

  defp collect_opts(%{name: name, children: children}, {out, path}) do
    children
    |> Enum.reduce({out, path ++ [name]}, &collect_opts/2)
    |> put_elem(1, path)
  end
  defp collect_opts(%Option{switches: []}, out), do: out
  defp collect_opts(%Option{name: name, switches: switches}, {out, path}) do
    out = switches
          |> Enum.partition(&(Regex.match?(~r/^--/, &1)))
          |> form_argv_opts(path ++ [name], out)
    {out, path}
  end

  defp form_argv_opts({[], aliases}, path, out) do
    aliases = argv_atoms(aliases)
    id = String.to_atom(Enum.join(path, "__"))
    out
    |> Keyword.put(:aliases, argv_put(aliases, id, out[:aliases]))
    |> Keyword.put(:strict,  Keyword.put(out[:strict], id, :string))
    |> Keyword.put(:refs,    Map.put(out[:refs], id, path))
  end
  defp form_argv_opts({strict, aliases}, path, out) do
    strict  = argv_atoms(strict)
    aliases = argv_atoms(aliases)
    id = strict |> List.first
    refs = strict
    |> Enum.map(&({&1, path}))
    |> Enum.into(%{})
    out
    |> Keyword.put(:aliases, argv_put(aliases, id, out[:aliases]))
    |> Keyword.put(:strict,  argv_put(strict, :string, out[:strict]))
    |> Keyword.put(:refs,    Map.merge(out[:refs], refs))
  end

  defp argv_atoms(list) do
    list
    |> Enum.map(&(String.trim(&1, "-")))
    |> Enum.map(&(String.replace(&1, "-", "_")))
    |> Enum.map(&String.to_atom/1)
  end

  defp argv_put(list, value, acc) do
    list
    |> Enum.reduce(acc, fn k, a -> Keyword.put(a, k, value) end)
  end
end
