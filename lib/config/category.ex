defmodule Config.Category do
  @moduledoc """
  Handles macroses to define config category.

  Following macroses available inside `category/2` block:

  - `Config.Container.type/1`: specifies category container type
  - `Config.Category.category/2`: adds sub category
  - `Config.Option.option/2`: adds option to category
  """

  alias Config.Container

  use Container

  @type t :: %__MODULE__ {
    name: Atom.t,
    type: Container.type,
    children: Container.children
  }

  defstruct [
    name: nil,
    type: :keyword,
    children: []
  ]

  @doc """
  Specifies configuration category

  Available only inside `Config.config/2` and other `Config.category/2` blocks.
  """
  defmacro category(name, opts) do
    case __MODULE__.compile(name, opts) do
      %__MODULE__{} = cat ->
        cat = Macro.escape(cat)
        quote do
          var!(config) = %{var!(config) |
            children: [unquote(cat) | var!(config).children]
          }
        end
      _ -> nil
    end
  end
end
