defmodule Config.Container do
  @moduledoc """
  Handles macroses used inside `Config.config/2` and
  `Config.Category.category/2` blocks.
  """

  alias Config.CompileError
  alias Config.Category
  alias Config.Option

  @type container_content :: [Category.t | Option.t]
  @type container_type    :: :keyword | :map
  @type t                 :: %{type: container_type,
                               children: container_content}

  @doc false
  @callback handle_compile(map, Keyword.t) :: map
  @doc false
  @callback handle_definition(any, map) :: map

  # private constants
  @types  [:keyword, :map]
  @macros [{Config.Category, [category: 2]},
           {Config.Option, [option: 1, option: 2, option: 3]},
           {__MODULE__, [type: 1]}]

  @doc """
  Specifies type for category or whole configuration.

  Supported values are: `:keyword` and `:map` or just `keyword` and `map`.

  Default is `:keyword`.
  """
  defmacro type(t) do
    case parse_type(t) do
      nil ->
        quote do
          raise(Config.CompileError,
                "Invalid type specified in #{var!(config).name}")
        end
      t ->
        quote do
          var!(config) = %{var!(config) | type: unquote(t)}
        end
    end
  end

  @doc false
  defmacro __using__(opts) do
    macros = Keyword.get(opts, :macros, [])

    quote do
      @behaviour unquote(__MODULE__)

      alias Config.CompileError
      import unquote(__MODULE__), only: [parse_type: 1, parse_name: 1]

      @doc false
      def compile(name, defs) do
        case parse_name(name) do
          nil ->
            raise(CompileError, "Invalid name specified")
          name ->
            container = struct(__MODULE__, name: parse_name(name))
            defs
            |> Enum.reduce(container, &parse_def/2)
            |> handle_compile(defs)
        end
      end

      # do block parsing
      defp parse_def({:do, block}, out) do
        macros = unquote(@macros) ++ unquote(macros)
        block
        |> Code.eval_quoted([config: out], macros: macros)
        |> elem(1)
        |> Keyword.get(:config)
      end
      # type parsing
      defp parse_def({:type, t}, %{type: _} = out) do
        case parse_type(t) do
          nil -> raise(CompileError, "Invalid type specified in #{out.name}")
          t   -> %{out | type: t}
        end
      end
      defp parse_def(definition, out) do
        handle_definition(definition, out)
      end

      @doc false
      def handle_compile(container, _), do: container

      @doc false
      def handle_definition(_, container), do: container

      defoverridable [handle_compile: 2, handle_definition: 2]
    end
  end

  @doc false
  def get(%{children: children}, [name]) do
    children |> Enum.find(&(&1.name == name))
  end
  def get(%{children: children}, [name | path]) do
    case children |> Enum.find(&(&1.name == name)) do
      nil   -> nil
      child -> child |> get(path)
    end
  end

  @doc false
  def put(%{children: _} = container, [name | _] = path, value, out) do
    container
    |> get([name])
    |> put_value(path, value, out)
  end
  def put(container, name, value, out) when is_atom(name) do
    put(container, [name], value, out)
  end

  @doc false
  def merge_into(env, out, %{children: children, type: type}) do
    case check_type(type).(env) do
      true ->
        has_key? = has_key(type)
        children
        |> Enum.filter(fn %{name: name} -> has_key?.(env, name) end)
        |> Enum.reduce(out, &(merge_value(&1, env, &2)))
      false ->
        out
    end
  end

  @doc false
  def create_value(:map), do: %{}
  def create_value(%{type: type}) when type in @types, do: create_value(type)
  def create_value(_), do: []

  @doc false
  def parse_name(name) when is_atom(name), do: name
  def parse_name({name, _, _}) when is_atom(name), do: name
  def parse_name(_), do: nil

  @doc false
  def parse_type(t) when t in @types, do: t
  def parse_type({t, _, _}) when t in @types, do: t
  def parse_type(_), do: nil

  defp merge_value(%{name: name, type: type} = config, env, out) when type in @types do
    value = case get_in(out, [name]) do
      nil   -> create_value(type)
      value -> value
    end
    put_in(out, [name], merge_into(get_in(env, [name]), value, config))
  end
  defp merge_value(%Option{name: name}, env, out) do
    put_in(out, [name], get_in(env, [name]))
  end

  defp put_value(nil, _, _, out), do: out
  defp put_value(%{type: type, children: _}, [_], _, out) when type in @types, do: out
  defp put_value(%{type: type, children: _} = config, [name | path], value, out) when type in @types do
    v = case get_in(out, [name]) do
      nil -> create_value(type)
      v   -> v
    end
    put_in(out, [name], put(config, path, value, v))
  end
  defp put_value(%Option{}, [name], value, out), do: put_in(out, [name], value)
  defp put_value(_, _, _, out), do: out

  defp has_key(:map), do: &Map.has_key?/2
  defp has_key(_),    do: &Keyword.has_key?/2

  defp check_type(:map),     do: &is_map/1
  defp check_type(:keyword), do: &Keyword.keyword?/1
end
