defmodule Config.Loader do
  @moduledoc false

  alias Config.Container
  alias Config.ParseError

  @type env_tuple :: {Config.env, Config.errors}

  @callback load(env_tuple, Container.t) :: env_tuple

  defmacro __using__(_opts) do
    quote do
      @behaviour unquote(__MODULE__)

      def load({env, errors}, _) do
        {env, [%ParseError{message: "#{__MODULE__}.load/2 is not implemented"}]}
      end

      defoverridable load: 2
    end
  end
end
