defmodule Config.Option do
  @moduledoc """
  Handles macroses to define config option.
  """

  alias Config.Option
  alias Config.CompileError

  @type t :: %Option {
    name: Atom.t,
    type: :string | :integer | :boolean | atom,
    default: any,
    env:  [String.t],
    switches: [String.t]
  }

  defstruct [
    name: nil,
    type: Config.String,
    default: Config.Undefined,
    env: [],
    switches: []
  ]

  @doc """
  Specifies configuration option.

  Available only inside `Config.config/2` and `Config.Category.category/2`
  blocks.

  `name` argument is required.

  `type` is `:string` by default. You can specify `str` or `string`,
  `int` or `integer`, `bool` or `boolean` here as atom or like a var name.
  Also you can define your own types and specify here module that uses
  `Config.Type`.

  last argument is options. It can be a list or a keyword.

  If options contains any underscored name as atom (`:some_opt`, `:n`), as
  string (`"some_opt"`, `"n"`) or as a var name (`some_opt`, `n`) this value
  treated as command-line switch name (`--some-opt` and `-n` for given example).
  One dash added if name length equal to 1. Moreover direct switch syntax
  (`"--some-opt"`, `"-n"`) in strings supported.

  If options contains a capitalized name (`:SOME_VAR`, `"SOME_VAR"` or just
  `SOME VAR`) this value treated as environment variable name.

  You can specify switches and environment variables with keyword syntax:
  `switch: "--some-opt -n", env: ~w(SOME_VAR)`.

  Default value specified with keyword key `default`, for example:
  `default: 100`.
  """
  defmacro option(name) do
    quote(do: option(unquote(name), Config.String, []))
  end
  defmacro option(name, option_type_or_opts) do
    case option_type_or_opts do
      opts when is_list(opts) ->
        quote(do: option(unquote(name), Config.String, unquote(opts)))
      option_type ->
        quote(do: option(unquote(name), unquote(option_type), []))
    end
  end
  defmacro option(name, option_type, opts) do
    case Option.compile(name, option_type, opts) do
      %Option{} = opt ->
        opt = Macro.escape(opt)
        quote do
          var!(config) = %{var!(config) |
            children: [unquote(opt) | var!(config).children]
          }
        end
      _ -> nil
    end
  end

  @doc false
  def compile(name, type, defs) do
    option = %Option{name: parse_name(name)} |> parse_type(type)
    defs |> Enum.reduce(option, &parse_def/2)
  end

  @type_aliases [:string, :integer, :boolean, :str, :int, :bool]
  @types %{string:  Config.String,
           str:     Config.String,
           integer: Config.Integer,
           int:     Config.Integer,
           boolean: Config.Boolean,
           bool:    Config.Boolean}
  @var_re ~r/^[A-Z][a-zA-Z0-9_]*$/
  @opt_re ~r/^(-{0,2})([a-zA-Z0-9-]+)$/
  @space_re ~r/\s+/

  # name parsing
  defp parse_name(name) when is_atom(name), do: name
  defp parse_name({name, _, _}) when is_atom(name), do: name
  defp parse_name(_), do: raise(CompileError, "Invalid name specified")

  # type parsing
  defp parse_type(out, type) when type in @type_aliases do
    %{out | type: @types[type]}
  end
  defp parse_type(out, {type, _, _}) when type in @type_aliases do
    %{out | type: @types[type]}
  end
  # custom type
  defp parse_type(out, type) when is_atom(type) do
    case function_exported?(type, :parse, 1) do
      true  -> %{out | type: type}
      false -> raise(CompileError, "Type module #{type} for option" <>
                                   " #{out.name} should have parse/1 function")
    end
  end
  defp parse_type(out, {:__aliases__, aliases, list}) do
    case Keyword.get(aliases, :alias) do
      false ->
        #:code.get_path |> Enum.map(&IO.inspect/1)
        type = list
               |> Enum.map(&Code.ensure_loaded/1)
               |> Enum.find_value(fn
                    {:module, type} -> type
                    _               -> false
                  end)
        case type do
          nil  -> out
          type -> parse_type(out, type)
        end
      type when is_atom(type) ->
        parse_type(out, type)
      _ ->
        out
    end
  end
  defp parse_type(%{name: name}, _) do
    raise(CompileError, "Invalid type specified in option #{name}")
  end

  # option name, type, ["-n --sw", "VAR"]
  defp parse_def(str, out) when is_bitstring(str) do
    case Regex.match?(@var_re, str) do
      true -> parse_env(str, out)
      _    -> parse_switch(str, out)
    end
  end
  # option name, type, [:switch]
  defp parse_def(:switch, %{name: name, switches: switches} = out) do
    case name |> to_string |> parse_switch do
      nil    -> out
      switch -> %{out | switches: Enum.uniq([switch | switches])}
    end
  end
  # option name, type, [switch]
  defp parse_def({:switch, _, _}, out), do: parse_def(:switch, out)
  # option name, type, switch: "--sw"
  defp parse_def({:switch, switch}, out) do
    case switch do
      false ->
        out
      true ->
        parse_def(:switch, out)
      switch when is_bitstring(switch) ->
        parse_switch(switch, out)
      switch when is_integer(switch) ->
        parse_switch(switch |> Integer.to_string, out)
      _ ->
        raise(CompileError, message: "Invalid switch value specified in" <>
                                     " option #{out.name}")
    end
  end
  # option name, type, env: "VAR"
  defp parse_def({:env, env}, out) do
    case env do
      env when is_bitstring(env) ->
        parse_env(env, out)
      _ ->
        raise(CompileError, message: "Invalid environment variable specified" <>
                                     " in option #{out.name}")
    end
  end
  # option name, type, [VAR]
  defp parse_def({:__aliases__, _, aliases}, out) when is_list(aliases) do
    aliases
    |> Enum.map(&to_string/1)
    |> Enum.reduce(out, &parse_def/2)
  end
  # option name, type, [sw]
  defp parse_def({atom, _, _}, out) when is_atom(atom) do
    parse_def(atom |> to_string, out)
  end
  # option name, type, default: value
  defp parse_def({:default, value}, out) do
    {value, _} = Code.eval_quoted(value, [])
    %{out | default: value}
  end
  # skip silently other definitions
  defp parse_def(_, out), do: out

  # option name, type, ["VAR1 VAR2"]
  defp parse_env(str, %{env: env} = out) do
    list = str
           |> String.trim
           |> String.split(@space_re)
    if Enum.any?(list, &(!Regex.match?(@var_re, &1))) do
      raise(CompileError, message: "Invalid environment variables \"#{str}\"" <>
                                   " specified in option #{out.name}")
    end
    %{out | env: Enum.uniq(env ++ list)}
  end

  # option name, type, ["--sw1 --sw2"]
  defp parse_switch(sw, %{switches: switches} = out) do
    list = sw
           |> String.trim
           |> String.split(@space_re)
           |> Enum.map(&parse_switch/1)
    if nil in list do
      raise(CompileError, message: "Invalid switch \"#{sw}\" specified in" <>
                                   " option #{out.name}")
    end
    %{out | switches: Enum.uniq(switches ++ list)}
  end

  defp parse_switch(sw) do
    sw = sw |> String.replace("_", "-")
    case Regex.run(@opt_re, sw, capture: :all_but_first) do
      ["", sw] -> if String.length(sw) > 1, do: "--#{sw}", else: "-#{sw}"
      [s, sw]  -> s <> sw
      _        -> nil
    end
  end
end
