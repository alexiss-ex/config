defmodule Config.String do
  @moduledoc """
  String type for config options.
  """

  use Config.Type

  @doc """
  Strings itself comes without parsing, all other types are passed to
  `Kernel.to_string/1` function.
  """
  @spec parse(any) :: {:ok, String.t}
  def parse(input) when is_bitstring(input), do: {:ok, input}
  def parse(input), do: {:ok, input |> to_string}
end
