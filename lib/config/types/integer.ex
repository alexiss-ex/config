defmodule Config.Integer do
  @moduledoc """
  Integer type for config options
  """

  use Config.Type

  @doc """
  Integers itself comes without parsing.

  For strings call to `String.to_integer/1` attempted. If no success, `:error`
  returned.

  Booleans treated as `1` for true and `0` for false.

  For any other values `:error` returned
  """
  @spec parse(any) :: {:ok, integer} | :error
  def parse(input) when is_integer(input), do: {:ok, input}
  def parse(input) when is_bitstring(input) do
    try do
      {:ok, String.to_integer(input)}
    rescue
      ArgumentError -> :error
    end
  end
  def parse(true),  do: {:ok, 1}
  def parse(false), do: {:ok, 0}
  def parse(float) when is_float(float), do: {:ok, round(float)}
  def parse(_), do: :error
end
