defmodule Config.Boolean do
  @moduledoc """
  Boolean type for config options
  """

  use Config.Type

  @doc """
  Booleans itself comes without parsing.

  Any integer not equal to `0` treated as `true`.

  Any string except `"0"`, `"no"` and `"false"` (case insensitive) treated as
  `true`

  For any other values `:error` returned
  """
  @spec parse(any) :: {:ok, boolean} | :error
  def parse(input) when is_boolean(input), do: {:ok, input}
  def parse(input) when is_integer(input) and input != 0, do: {:ok, true}
  def parse(input) when is_integer(input), do: {:ok, false}
  def parse("0"), do: {:ok, false}
  def parse(input) when is_bitstring(input) do
    {:ok, !Regex.match?(~r/^(false|no)$/i, input)}
  end
  def parse(_), do: :error
end
