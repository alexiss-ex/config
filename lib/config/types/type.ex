defmodule Config.Type do
  @moduledoc """
  Behaviour module for defining custom configuration options types
  """
  use Behaviour

  @doc """
  Called after retrieving value from any source to convert it into internal
  representation before putting into application environment.

  Return `{:ok, value}` on success, `:ignore` to skip this value silently,
  `:error` or `{:error, msg}` to raise ParseError
  """
  @callback parse(any) :: {:ok, any} | :ignore | :error | {:error, String.t}

  defmacro __using__(_opts) do
    quote do
      @behaviour unquote(__MODULE__)

      def parse(_) do
        {:error, "value parsing of #{__MODULE__} type is not implemented."}
      end

      defoverridable [parse: 1]
    end
  end
end
