defmodule Config.Configuration do
  @moduledoc """
  Handles macroses used inside `Config.config/2` block.

  Beside macroses, defined in this module following macroses also available
  in `Config.config/2` block:

  - `Config.Container.type/1`: specifies configuration container type
  - `Config.Category.category/2`: adds config category
  - `Config.Option.option/2`: adds config option
  """

  alias Config.Container
  alias Config.Loader
  alias Config.Option

  use Container, macros: [{__MODULE__, [add_file: 1, add_path: 1, files: 1,
                                        paths: 1, remove_file: 1,
                                        remove_path: 1]}]
  use Loader

  @type t :: %__MODULE__ {
    name:     Atom.t,
    type:     Container.container_type,
    paths:    [String.t],
    files:    [String.t],
    children: Container.container_content
  }

  defstruct [
    name:     nil,
    type:     :keyword,
    paths:    [],
    files:    [],
    children: []
  ]

  @doc """
  Adds search path where config files will be searched.

  Relative paths expanded using current folder.
  """
  defmacro add_path(path) do
    quote do
      var!(config) = case unquote(path) do
        path when is_bitstring(path) ->
          path = Path.expand(path)
          %{var!(config) | paths: [path | var!(config).paths]}
        _ ->
          var!(config)
      end
    end
  end

  @doc """
  Removes path from search paths list where config files will be searched.

  Relative paths expanded using current folder.
  """
  defmacro remove_path(path) do
    quote do
      var!(config) = case unquote(path) do
        path when is_bitstring(path) ->
          path = Path.expand(path)
          %{var!(config) | paths: [List.delete(var!(config).paths, path)]}
        _ ->
          var!(config)
      end
    end
  end

  @doc """
  Resets search paths list where config files will be searched.

  Relative paths expanded using current folder.

  By default search paths consists of current folder, user home folder and
  application folder.
  """
  defmacro paths(paths) do
    quote do
      var!(config) = case unquote(paths) do
        paths when is_list(paths) ->
          paths = paths
                  |> Enum.uniq
                  |> Enum.filter(fn
                    path when is_bitstring(p) -> true;
                    _ -> false
                  end)
                  |> Enum.map(&Path.expand/1)
          %{var!(config) | paths: [path | var!(config).paths]}
        _ ->
          var!(config)
      end
    end
  end

  @doc """
  Adds config file pattern.

  Patterns are evaluated at runtime with `name` local variable that contains
  configuration name and `ext` that contains file extension (without leading
  dot). To use this variables in file name use something like this:
  `"\#{var!(name)}.\#{var!(ext)}"`.
  """
  defmacro add_file(file) do
    file = Macro.escape(file)
    quote do
      var!(config) = %{var!(config) |
        files: [unquote(file) | var!(config).files]
      }
    end
  end

  @doc """
  Removes config file pattern.
  """
  defmacro remove_file(file) do
    file = Macro.escape(file)
    quote do
      var!(config) = %{var!(config) |
        files: List.delete(var!(config).files, unquote(file))
      }
    end
  end

  @doc """
  Resets config file pattern list.

  By default file list contains one value: `".\#{name}.\#{ext}"`.
  """
  defmacro files(files) do
    files = files |> Enum.map(&Macro.escape/1)
    quote do
      var!(config) = %{var!(config) | files: unquote(files)}
    end
  end

  @doc false
  def handle_compile(config, _) do
    config
    |> ensure_paths
    |> ensure_files
  end

  @doc false
  def load({env, errors}, %{children: children} = config) do
    env = children
          |> Enum.reduce(Container.create_value(config), &load_from/2)
          |> Container.merge_into(env, config)
    {env, errors}
  end

  defp load_from(%{name: name, type: type, children: children}, out) do
    value = children |> Enum.reduce(Container.create_value(type), &load_from/2)
    put_in(out, [name], value)
  end
  defp load_from(%Option{name: name, default: value}, out) do
    put_in(out, [name], value)
  end

  defp ensure_paths(%{name: name, paths: []} = config) do
    paths = [System.get_env("HOME"), File.cwd!]
    paths = try do
      [Application.app_dir(name) | paths]
    rescue
      ArgumentError -> paths
    end
    paths = for path when is_bitstring(path) <- Enum.uniq(paths), do: path
    %{config | paths: paths}
  end
  defp ensure_paths(config), do: config

  defp ensure_files(%{files: []} = config) do
    %{config | files: [quote(do: ".#{var!(name)}.#{var!(ext)}")]}
  end
  defp ensure_files(config), do: config
end
