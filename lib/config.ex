defmodule Config do
  @moduledoc """
  Provides convenient way to load application configuration from config files
  and environment variables.

  Quickstart example:

      defmodule MyApplication do
        use Application
        use Config

        Config.config :my_app do
          option timeout, int, ["-t", "--timeout", "MY_TIMEOUT", default: 100]
          category connection do
            type   :map
            option host, string, [h, host, MY_HOST, default: "localhost"]
          end
        end

        def start(_type, _args) do
          Config.load(:my_app)
          Application.get_env(:my_app, :timeout)    # -> 100
          Application.get_env(:my_app, :connection) # -> %{host: "localhost"}
        end
      end

  Options, available inside config block:

  - `Config.Configuration.add_path/1`: adds path to a list of search paths
    where config files will be searched
  - `Config.Configuration.remove_path/1`: removes path from search path list
  - `Config.Configuration.paths/1`: resets search path list
  - `Config.Configuration.add_file/1`: adds config file pattern
  - `Config.Configuration.remove_file/1`: remove config file pattern
  - `Config.Configuration.files/1`: resets config file pattern list
  - `Config.Container.type/1`: specifies configuration container type
  - `Config.Category.category/2`: adds config category
  - `Config.Option.option/2`: adds config option
  """

  @type env    :: map | keyword
  @type errors :: [Exception.t]

  defmodule Undefined do
    @moduledoc false
  end

  @apps __MODULE__.Applications

  alias Config.Configuration
  alias Config.CommandLine
  alias Config.ConfigFile
  alias Config.Env

  @doc """
  Loads configuration for specified application.

  Returns `{:ok, config}` tuple with loaded configuration on success or
  `{:error, list}` with list of errors.
  """
  @spec load(Configuration.t, atom) :: {:ok, keyword | map} | {:error, errors}
  def load(module, app) when is_atom(module) and is_atom(app) do
    if function_exported?(module, :__config__, 1) do
      config = module.__config__(app)
      {env, errors} = {Application.get_all_env(app), []}
                      |> Configuration.load(config)
                      |> ConfigFile.load(config)
                      |> CommandLine.load(config)
                      |> Env.load(config)
      case errors do
        [] ->
          Enum.each(env, fn {k, v} -> Application.put_env(app, k, v) end)
          :ok
        errors ->
          {:error, errors}
      end
    else
      msg = "Invalid config module specified as a first argument"
      {:error, [%ArgumentError{message: msg}]}
    end
  end

  @doc """
  Loads configuration for specified application or raises error.

  Bang version of `load/2`.
  """
  @spec load(Configuration.t, atom) :: {:ok, env} | no_return
  def load!(module, app) when is_atom(module) and is_atom(app) do
    with {:error, [error | _]} <- load(module, app) do
      raise(error)
    end
  end

  @doc """
  Specifies configuration that will be used in `Config.load/2` function.

  See `Config.Configuration` for all options, available inside config block.
  """
  defmacro config(app, opts \\ []) do
    case Configuration.compile(app, opts) do
      %Configuration{} = config ->
        app = {app, Macro.escape(config)}
        quote do
          Module.put_attribute(__MODULE__, unquote(@apps), unquote(app))
        end
      _ ->
        quote do
        end
    end
  end

  @doc false
  defmacro __using__(_opts) do
    quote do
      require unquote(__MODULE__)
      Module.register_attribute(__MODULE__, unquote(@apps), accumulate: true)
      @before_compile unquote(__MODULE__)
    end
  end

  @doc false
  defmacro __before_compile__(env) do
    apps = env.module
           |> Module.get_attribute(@apps)
           |> collect_apps(%{})
           |> Macro.escape
    quote do
      def __config__,      do: unquote(apps)
      def __config__(app), do: Map.get(unquote(apps), app)
    end
  end

  @doc false
  defmacro search_for(expr) do
    quote do
      fn unquote(expr) -> true; _ -> false end
    end
  end

  # deep merge configurations
  defp collect_apps([], apps), do: apps
  defp collect_apps([{app, conf} | rest], apps) do
    %{}
    |> Map.put(app, conf)
    |> Map.merge(collect_apps(rest, apps))
  end
end
