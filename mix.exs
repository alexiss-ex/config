defmodule Config.Mixfile do
  use Mix.Project

  def project do
    [app: :config,
     version: "0.1.0",
     elixir: "~> 1.3",
     elixirc_paths: paths(Mix.env),
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     description: description(),
     package: package(),
     deps: deps(),
     #test_coverage: [tool: ExCoveralls],
     #test_coverage: [tool: Coverex.Task],
     #preferred_cli_env: ["coveralls": :test,
     #                    "coveralls.detail": :test,
     #                    "coveralls.post": :test,
     #                    "coveralls.html": :test],
     docs: [extras: ["README.md"]]]
  end

  def application do
    [applications: applications(Mix.env)]
  end

  defp deps do
    [#{:excoveralls, "~> 0.5", only: :test},         # Test coverage
     {:yamerl, "~> 0.3", github: "yakaz/yamerl", only: [:dev, :test]}, # YAML
     {:ex_doc, "~> 0.12", only: :dev},              # Docs generation
     {:dialyxir, "~> 0.3.5", only: [:dev]},         # Code analysis
     {:credo, "~> 0.4", only: [:dev, :test]}]       # Code style
  end

  defp paths(:test), do: ["lib", "test/support"]
  defp paths(_),     do: ["lib"]

  def applications(env) when env in [:dev, :test], do: [:logger, :yamerl]
  def applications(_),                             do: [:logger]

  defp description do
    """
    Provides convenient way to load application configuration from config files
    and environment variables.
    """
  end

  # https://hex.pm/docs/publish
  defp package do
    [name: :config,
     files: ["lib", "mix.exs", "README*", "LICENSE*"],
     maintainers: ["Alexey Ovchinnikov <alexiss@sferosoft.ru>"],
     licenses: ["MIT"],
     links: %{"GitLab" => "https://gitlab.com/alexiss-ex/config",
              "Docs"   => "http://docs.sferosoft.ru/config"}]
  end
end
