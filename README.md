[![build status](http://git.sferosoft.ru/opensource/config/badges/master/build.svg)](http://git.sferosoft.ru/opensource/config/commits/master)

# Config

Provides convenient way to load application configuration from config files
and environment variables.

## Installation

Add config to your list of dependencies in `mix.exs`:

    def deps do
      [{:config, "~> 0.1.0"}]
    end

## Usage

> **WARNING!** Project is in active development stage. Use it carefully in
> production. Coming `v1.0.0` will be first stable release.

Please refer to [package documentation](http://docs.sferosoft.ru/config/Config.html)
for details.

Specify your configuration (it handy to do it in your application module) by
adding `use Config` and using `Config.config/2` macro. After that you can load
configuration with `Config.load/2` function like in following example:

```elixir
defmodule MyApplication do
  use Application
  use Config

  Config.config :my_application do
    option timeout, int, ["-t", "--timeout", "MY_TIMEOUT", default: 100]
    category connection do
      type   :map
      option host, string, [h, host, MY_HOST, default: "localhost"]
    end
  end

  def start(_type, _args) do
    Config.load(:my_application)
    Application.get_env(:my_application, :timeout)    # -> 100
    Application.get_env(:my_application, :connection) # -> %{host: "localhost"}
  end
end
```

Firstly `Config.load/2` fills application environment with default values.

After that it will search for `.my_application.yml` in current folder, user
home folder and application folder and if success loads config from it into
application environment, overriding existing values.

After that it will parse command-line options and overrides corresponding
values (`timeout` if `-t` or `--timeout` option specified and
`connection.host` if `-h` or `--host` option specified).

Finally, environment variables will be scanned for `MY_TIMEOUT` and `MY_HOST`
and if present their values will override corresponding configuration.

## TODO

- [ ] Parsing of common types like ip addresses and so on
- [ ] JSON file support
- [ ] INI file support
